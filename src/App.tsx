import React from "react";
import "./App.scss";
import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import Scheduler from "./components/scheduler/Scheduler";

const App: React.FC = () => (
  <div>
    <Header />
    <div className="container">
      <div className="grid-container">
        <Scheduler />
      </div>
      <Footer />
    </div>
  </div>
);

export default App;
