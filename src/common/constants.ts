export const TIME_INTERVAL = 0.5; // 30 mins interval between each appointment

export const MORNING_OFFICE_HOURS = {
  hours: {
    startTime: 8,
    endTime: 14,
  },
  break: {
    startTime: 11,
    endTime: 11.5,
  },
};

export const AFTERNOON_OFFICE_HOURS = {
  hours: {
    startTime: 13,
    endTime: 19,
  },
  break: {
    startTime: 16,
    endTime: 16.5,
  },
};

export const MAX_NUMBER_APPOINTMENTS_PER_WEEK = 2;
