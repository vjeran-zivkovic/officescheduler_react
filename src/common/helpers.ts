import {
  MORNING_OFFICE_HOURS,
  AFTERNOON_OFFICE_HOURS,
  TIME_INTERVAL,
} from "./constants";

export enum PeriodTypeEnum {
  Break,
  Taken,
  Selected,
  Available,
}

export interface IPeriod {
  startDate: Date,
  endDate: Date,
  periodType: PeriodTypeEnum,
}

export function getSchedulerDatesStartingTomorrow(): Date[] {
  function getNthDayFromToday(n: number): Date {
    const date: Date = new Date();
    date.setDate(date.getDate() + n);
    return date;
  }

  const schedulerDays: Date[] = [];
  // special case when tomorrow is Sunday (2 days in a week would not be be shown) --> start from Monday
  const n = getNthDayFromToday(1).getDay() === 0 ? 9 : 8;
  for (let i = 1; i <= n; i++) {
    const date: Date = getNthDayFromToday(i);
    // office is always closed on sundays
    if (date.getDay() !== 0) {
      schedulerDays.push(date);
    }
  }

  return schedulerDays;
}

/**
 * Returns array of scheduler periods grouped into arrays ordered by time.
 */
export function getSchedulerPeriods(): IPeriod[][] {
  // flatten ordered periods in order to be able to pick random periods
  const flattenedPeriods: IPeriod[] = getSchedulerPeriodsOrderedByTime().flat();
  return getSchedulerPeriodsWithRandomlyTakenPeriods(flattenedPeriods);
}

export function getDateString(date: Date): string {
  const options = { weekday: 'long', month: 'numeric', day: 'numeric' };
  return new Intl.DateTimeFormat("hr-HR", options).format(date);
}

export function getTimeString(period: IPeriod): string {
  const options = { hour: 'numeric', minute: 'numeric' };
  return `${new Intl.DateTimeFormat("hr-HR", options).format(
    period.startDate
  )} - ${new Intl.DateTimeFormat("hr-HR", options).format(period.endDate)}`;
}

function getDateWithTime(date: Date, hours: number, minutes: number): Date {
  const clonedDate = new Date(date.getTime());
  clonedDate.setHours(hours, minutes, 0);
  return clonedDate;
}

function getSchedulerPeriodsOrderedByTime(): IPeriod[][] {
  const periodsOrderedByTime: IPeriod[][] = [];
  for (let t = MORNING_OFFICE_HOURS.hours.startTime; t < AFTERNOON_OFFICE_HOURS.hours.endTime; t += TIME_INTERVAL) {
    const periods: IPeriod[] = [];
    getSchedulerDatesStartingTomorrow().forEach((date: Date) => {
      const startHours = t;
      const startMinutes = (startHours % 1) * 60;
      const endHours = startHours + TIME_INTERVAL;
      const endMinutes = (endHours % 1) * 60;
      const period: IPeriod = {
        startDate: getDateWithTime(date, startHours, startMinutes),
        endDate: getDateWithTime(date, endHours, endMinutes),
        periodType: PeriodTypeEnum.Available,
      };

      if (t >= MORNING_OFFICE_HOURS.hours.startTime && t < MORNING_OFFICE_HOURS.hours.endTime
        && date.getDate() % 2 === 0) {
        if (t === MORNING_OFFICE_HOURS.break.startTime) {
          period.periodType = PeriodTypeEnum.Break;
        }
        periods.push(period);
      } else if (t >= AFTERNOON_OFFICE_HOURS.hours.startTime && t < AFTERNOON_OFFICE_HOURS.hours.endTime
        && date.getDate() % 2 === 1
        && date.getDay() !== 6) { // saturday restriction
        if (t === AFTERNOON_OFFICE_HOURS.break.startTime) {
          period.periodType = PeriodTypeEnum.Break;
        }
        periods.push(period);
      }
    })

    periodsOrderedByTime.push(periods);
  }

  return periodsOrderedByTime;
}

function getSchedulerPeriodsWithRandomlyTakenPeriods(flattenedPeriods: IPeriod[]): IPeriod[][] {
  const takenPeriods: IPeriod[] = [];
  for (let i = 0; i < 15; i++) {
    const randomIndex: number = Math.floor(Math.random() * flattenedPeriods.length);
    const randomPeriod: IPeriod = flattenedPeriods[randomIndex];
    // if random period was already taken or if it is a break, try again
    if (takenPeriods.includes(randomPeriod) || randomPeriod.periodType === PeriodTypeEnum.Break) {
      i--;
      continue;
    }
    randomPeriod.periodType = PeriodTypeEnum.Taken;
    takenPeriods.push(randomPeriod);
  }

  const schedulerPeriodsWithRandomlyTakenPeriods: IPeriod[][] = [];
  let groupedPeriods: IPeriod[] = [];
  let currentPeriod: string = getTimeString(flattenedPeriods[0]);
  // group periods back into array of grouped periods by time 
  flattenedPeriods.forEach((period: IPeriod) => {
    if (getTimeString(period) === currentPeriod) {
      groupedPeriods.push(period);
    } else {
      currentPeriod = getTimeString(period);
      schedulerPeriodsWithRandomlyTakenPeriods.push(groupedPeriods);
      groupedPeriods = [];
      groupedPeriods.push(period);
    }
  })
  schedulerPeriodsWithRandomlyTakenPeriods.push(groupedPeriods);

  return schedulerPeriodsWithRandomlyTakenPeriods;
}
