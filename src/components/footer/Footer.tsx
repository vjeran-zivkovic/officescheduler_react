import React from "react";

const Footer: React.FC = () => (
  <div className="footer">
    <div className="legend">
      <div className="square closed"></div><div>Neradno vrijeme</div>
      <div className="square available"></div><div>Dostupan termin</div>
      <div className="square taken"></div><div>Zauzet termin</div>
      <div className="square selected"></div><div>Odabrani termin</div>
      <div className="square break"></div><div>Pauza</div>
    </div>
    
    <p>
      <b>Napomena:&nbsp;</b>Ordinacija ne radi vikendom, osim parnih subota u jutarnjem terminu.
      Moguće je odabrati maksimalno do 2 termina u tjednu, i do 1 termin u danu.
    </p>
  </div>
)

export default Footer;
