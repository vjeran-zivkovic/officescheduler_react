import React from "react";
import {
  getSchedulerPeriods,
  getSchedulerDatesStartingTomorrow,
  getDateString,
  getTimeString,
  IPeriod,
  PeriodTypeEnum,
} from "../../common/helpers";
import SchedulerItem from "./SchedulerItem";
import { MAX_NUMBER_APPOINTMENTS_PER_WEEK } from "../../common/constants";

interface IState {
  schedulerDatesStartingTomorrow: Date[],
  schedulerPeriods: IPeriod[][];
  selectedPeriods: IPeriod[];
}

class Scheduler extends React.PureComponent<{}, IState> {
  state = {
    schedulerDatesStartingTomorrow: [] as Date[],
    schedulerPeriods: [] as IPeriod[][],
    selectedPeriods: [] as IPeriod[],
  }

  componentDidMount() {
    this.setState({
      schedulerDatesStartingTomorrow: getSchedulerDatesStartingTomorrow(),
      schedulerPeriods: getSchedulerPeriods(),
    });
  }

  onItemClick = (clickedPeriod: IPeriod) => {
    if (clickedPeriod.periodType !== PeriodTypeEnum.Selected) {
      if (this.state.selectedPeriods.some((period: IPeriod) =>
        period.startDate.getDate() === clickedPeriod.startDate.getDate())) {
          window.alert('Moguće je odabrati najviše 1 termin u danu.');
          return;
        }

      if (this.state.selectedPeriods.length === MAX_NUMBER_APPOINTMENTS_PER_WEEK) {
        window.alert('Moguće je odabrati najviše 2 termina u tjednu.');
        return;
      }
    }

    this.setState((prevState: IState) => ({
      selectedPeriods:
        clickedPeriod.periodType !== PeriodTypeEnum.Selected
          ? [...prevState.selectedPeriods, clickedPeriod]
          : prevState.selectedPeriods
            .filter((period: IPeriod) => period.startDate !== clickedPeriod.startDate),

      schedulerPeriods: prevState.schedulerPeriods.map((groupedPeriods: IPeriod[]) => {
        return groupedPeriods.map((period: IPeriod) => {
          if (period === clickedPeriod) {
            return {
              ...period,
              periodType: period.periodType === PeriodTypeEnum.Selected
                ? PeriodTypeEnum.Available
                : PeriodTypeEnum.Selected
            };
          } else {
            return period;
          }
        });
      }),
    }))
  }

  render() {
    const {
      schedulerDatesStartingTomorrow,
      schedulerPeriods,
    } = this.state;

    return (
      <>
        <div className="corner-grid-item"></div>

        {schedulerDatesStartingTomorrow.map((day: Date) => (
          <div key={day.getDate()} className="date-grid-item">{getDateString(day)}</div>
        ))}

        {schedulerPeriods.map((periods: IPeriod[]) => {
          let pointer: number = 0;
          const timeString = getTimeString(periods[0]);
          return [
            <div key={timeString} className="time-grid-item">{timeString}</div>,
            // for every ordered group of periods match a period with a date and render an item
            ...schedulerDatesStartingTomorrow.map((date: Date) => {
              const dateNumber = date.getDate();
              const period: IPeriod = periods[pointer];
              if (pointer < periods.length && period.startDate.getDate() === date.getDate()) {
                pointer++
                return (
                  <SchedulerItem
                    key={dateNumber}
                    period={period}
                    onItemClick={this.onItemClick}
                  />
                );
              } else {
                return <div key={dateNumber} className="closed-grid-item" />;
              }
          })]
        })}
      </>
    )
  }
}

export default Scheduler;
