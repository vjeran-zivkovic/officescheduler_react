import React from "react";
import { IPeriod, PeriodTypeEnum } from "../../common/helpers";

interface IProps {
  period: IPeriod;
  onItemClick: (clickedPeriod: IPeriod) => void;
}

const getClassName = (period: IPeriod) => {
  switch (period.periodType) {
    case PeriodTypeEnum.Break:
      return "break";
    case PeriodTypeEnum.Taken:
      return "taken";
    case PeriodTypeEnum.Selected:
      return "selected";
    default:
      return "available";
  }
}

const SchedulerItem: React.FC<IProps> = (props: IProps) => {
  const { period, onItemClick } = props;
  const isItemDisabled = period.periodType === PeriodTypeEnum.Break ||  period.periodType === PeriodTypeEnum.Taken;

  return (
    <button
      className={`btn-grid-item ${getClassName(props.period)}`}
      onClick={isItemDisabled
        ? () => { return; }
        : () => onItemClick(props.period)
      }
      disabled={isItemDisabled}
    />
  );
}

export default SchedulerItem;
