import React from "react";

const Header: React.FC = () => (
  <div className="header">
    <h1>Kalendar radnog vremena ordinacije</h1>
  </div>
)

export default Header;
